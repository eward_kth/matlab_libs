minority_samples = [1.2 3.2;
        1.5 3.3;
        3.2 2.3;
        2.7 2.6;
        3.8 4.2;
        0.7 3.1;
        3.2 0.9;
        -0.1 1.2;
        2.3  0.7];

figure; hold on;    
plot(minority_samples(:,1), minority_samples(:,2), 'o');

%%
N = 100;
k = 3;
synt_data = SMOTE(minority_samples,N,k);

%%
plot(synt_data(:,1),synt_data(:,2),'rx');
