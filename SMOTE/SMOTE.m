function [ synthetic_samples ] = SMOTE( minority_samples, N, k )
%SMOTE Synthetic Minority Oversampling TEchnique
%   minority samples - nxd matrix of n d-dimensional samples that we wish to
%   synthetically oversample.
%   N - oversampling fraction - should be an integer >= 1
%   k - numer of nearest neighbours evaluated to select one neighbour used
%       for interpolating

%%
if( N < 1)
    warning('N less than 100%, will do nothing');
    synthetic_samples = minority_samples;
    return;
end

%for each minority sample we create this number of synthetic samples
if N ~= round(N)
   warning('N is not an integer, rounded to nearsest integer!'); 
end
N = round(N);

if k ~= round(k) && k < 2
    warning('k must be an integer >= 2!, k set to 4');
    k = 4;
end

%Use matlabs knnsearch to efficently search for neighbours to each
%datapoint. nnmatrix contains a row of indexes of the nearest neighbours for
%each minority sample

%the indexes are sorted in order of closest distance
nnmatrix = knnsearch(minority_samples,minority_samples, 'K',k+1,'NSMethod','kdtree');



n = size(minority_samples,1);
d = size(minority_samples,2);

synthetic_samples = zeros(n*N,d);
%%
for i=1:n %1:n
    %%
    %select one of the neigbours (not the datpoint itself)
    nnmatrix_cols = randi([2 k+1],1,N); %which columns for row i in nnmatrix    
    nn_idx = nnmatrix(i,nnmatrix_cols);
    nn = minority_samples(nn_idx,:);
    
    
    original = repmat(minority_samples(i,:),N,1);    
    diff = nn - original;
                    
    %random number between 0,1 for each dimension, for each synthetic
    %sample
    gap = rand(N,d);
    
    syn_samp = original + gap .* diff;
    
    %%
    synthetic_samples( (i-1)*N+1 :  i*N   ,  :) = syn_samp; 
    
end


end

