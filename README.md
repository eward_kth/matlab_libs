# Matlab libs

Matlab implementations of various machine learning / probabilistic inference algorithms.

## Preprocessing

1. SMOTE - Synthetic Minority Oversampling TEchinique


## Others

1. (Gaussian) Kernel density esimation. Likelihood function by having
a kernel for each training example (particle) and treating it as a
gaussian mixture model with eaqual weight for each training example.

